#!/bin/bash

echo "Masukkan Username Anda :"
read username

if ! grep -q "^${username}/" /users/users.txt; then
    echo "Username Tidak Valid !!!"
else
    userpw=$(grep "^${username}" /users/users.txt | cut -d'/' -f2)
    echo "Masukkan Password Anda :"
    read password

    if [[ "$password" != "$userpw" ]]; then
        echo "Password Salah !!!"
        echo "date=$(date '+%m-%d-%Y %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> ./log.txt
    else
        echo "Login Berhasil"
        echo "date=$(date '+%m-%d-%Y %H:%M:%S') LOGIN: INFO User $username logged in" >> ./log.txt
    fi
fi

