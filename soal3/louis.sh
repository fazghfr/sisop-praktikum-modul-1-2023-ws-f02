#!/bin/bash

echo "Masukkan Username Anda :"
read username

if grep -q "^${username}/" /users/users.txt 
then
    echo "Username Tidak Valid !!!"
    echo "REGISTER: ERROR User already exists" >> ./log.txt
else
    echo "Username Telah Dimasukkan"
    
    echo "-------------------------"
    
    echo "Masukkan Password Anda :"
    read password
 
    if [[ ${#password} -gt 7 && "$password" =~ [A-Z] &&  "$password" =~ [a-z]  &&  "$password" =~ [0-9]  && "$password" != "$username"  &&  "$password" != *"chicken"*  &&  "$password" != *"ernie"* ]]  
    then
        echo "Selamat Password Memenuhi"
        echo "date=$(date '+%m-%d-%Y %H:%M:%S') REGISTER: INFO User $username registered successfully" >> ./log.txt
        echo $username/$password >> /users/users.txt

    else
        echo "Maaf Password Tidak Memenuhi !!!"
    fi
fi

