# Sistem Operasi F - Laporan Resmi Shift 1
Kelas Sistem Operasi F - Kelompok F02

## Anggota Kelompok :
- Salsabila Fatma Aripa (5025211057)
- Arfi Raushani Fikra (5025211084)
- Ahmad Fauzan Alghifari (5025211091)

### Soal 1

**a. Dari dataset.csv yang diberikan, tampilkan 5 universitas dengan ranking tertinggi di Jepang**
```sh
grep 'JP' dataset.csv | sort -t, -n -k1| head -5 | awk -F ',' 'BEGIN {print "Top 5 University in Japan"} {print $1, $2}'
```
![output_1](/uploads/880238ada528eb04c1c1dda31f45ecbf/output_1.png)

Penjelasan :
1. `grep 'JP' dataset.csv` digunakan untuk mengambil data universitas dengan kode lokasi `JP` atau Jepang.

2. `sort -t, -n -k1` digunakan untuk meng-sort data. `-t,` disini untuk memperjelas delimiternya adalah koma, `n` untuk mengspesifikasikan sort dilakukan secara numerical, dan `-k1` digunakan untuk menyatakan bahwa sorting yang dilakukan berdasarkan kolom pertama (kolom ranking).

3. `head -5` digunakan untuk mengambil 5 data saja, dari hasil command sebelumnya.

4. `awk -F ',' 'BEGIN {print "Top 5 University in Japan"} {print $1, $2}' ` Digunakan untuk mencetak hasil menggunakan `awk`. `F` digunakan untuk menyataskan delimiter, `'BEGIN {print "Top 5 University in Japan"} {print $1, $2}'` Command disamping digunakan untuk mencetak "Top 5 University in Japan" terlebih dahulu, kemudian baru mencetak data data yang dihasilkan, dengan kolom yang dicetak hanya kolom pertama dan kedua.


**b. Dari dataset.csv, tampilkan 5 universitas di jepang dengan skor fsr terendah**
```sh
grep 'JP' dataset.csv | sort -t, -g -k9| head -5|awk -F',' 'BEGIN{print "\nLowest FSR Score University in Japan"; print "FSR, NAME"}{print $9,$2}' 
```
![output_2](/uploads/b4b9cb62d173d0a185607934f23af957/output_2.png)

Penjelasan :
1. `grep 'JP' dataset.csv` digunakan untuk mengambil data universitas dengan kode lokasi `JP` atau Jepang.

2. `sort -t, -g -k9` digunakan untuk meng-sort data, `-t` digunakan untuk memperjelas delimiter (pada kasus ini koma), `-g` digunakan untuk menyatakan bahwa proses sort dilakukan secara general. Hal ini dikarenakan data pada kolom ke-sembilan adalah data float atau double. Terakhir, `-k9` digunakan untuk meng-spesifikasikan bahwa sortir dilakukan berdasarkan kolom ke-sembilan.

3. `head -5` digunakan untuk mengambil 5 data saja, dari hasil command sebelumnya.

4.  `awk -F',' 'BEGIN{print "\nLowest FSR Score University in Japan"; print "FSR, NAME"}{print $9,$2}'` Digunakan untuk mencetak hasil menggunakan `awk`. `F` digunakan untuk menyataskan delimiter, `'BEGIN{print "\nLowest FSR Score University in Japan"; print "FSR, NAME"}{print $9,$2}'` Command disamping digunakan untuk mencetak "\nLowest FSR Score University in Japan" dan "FSR, NAME" terlebih dahulu, kemudian baru mencetak data data yang dihasilkan, dengan kolom yang dicetak hanya kolom kesembilan dan kedua.


**c. Menampilkan 10 Universitas di jepang dengan Employment Outcome Rank tertinggi**
```sh
grep 'JP' dataset.csv | sort -t, -g -k20 | head -10 | awk -F',' 'BEGIN{print "\nTop 10 university based  on GER rank"}{print $1,$20,$2}'
```
![output_3](/uploads/383b04392a20126c22643ba6aa01047a/output_3.png)


Penjelasan :
1. `grep 'JP' dataset.csv` digunakan untuk mengambil data universitas dengan kode lokasi `JP` atau Jepang.

2. `sort -t, -g -k20` digunakan untuk meng-sort data, `-t` digunakan untuk memperjelas delimiter (pada kasus ini koma), `-g` digunakan untuk menyatakan bahwa proses sort dilakukan secara general. Hal ini dikarenakan data pada kolom ke-sembilan adalah data float atau double. Terakhir, `-k20` digunakan untuk meng-spesifikasikan bahwa sortir dilakukan berdasarkan kolom ke-20.

3. `head -10` digunakan untuk mengambil 10 data saja, dari hasil command sebelumnya.

4. `awk -F',' 'BEGIN{print "\nTop 10 university based  on GER rank"}{print $1,$20,$2}'` digunakan untuk mencetak menggunakan `awk`. Kemudian `-F','` digunakan untuk meng-spesifikasikan delimiter. kemudian `'BEGIN{print "\nTop 10 university based  on GER rank"}{print $1,$20,$2}'` digunakan untuk mencetak hasil dengan awk, dan akan dicetak "\nTop 10 university based  on GER rank" terlebih dahulu dan selanjutnya akan dicetak data hasilnya, dengan yang dicetak adalah kolom 1,20, dan 2.


**d. mencari universitas tersebut dengan kata kunci keren.**
```sh
grep 'Keren' dataset.csv | awk -F ',' '{print $1,$2}'
```
![output_4](/uploads/51e2204e5612fd222f9bb35eace4ba33/output_4.png)

Penjelasan : 
1. `grep 'Keren' dataset.csv` digunakan untuk mencari data dengan kata kunci `Keren`.

2. `awk -F ',' '{print $1,$2}'` digunakan untuk mencetak hasil, dengan yang dicetak hanya kolom pertama dan kedua.


### Soal 2
**a.** Download foto tentang indonesia, sebanyak n kali setiap 10 jam. n disini adalah jam sekarang. Kemudian simpan foto-foto tersebut pada folder kumpulan_x, dimana x adalah urutan foldernya. Foto-foto tersebut dinamakan dengan format perjalanan_x, dimana x adalah urutan download. Kondisi khusus, ketika jam 00:00, image yang didownload hanya 1.

**Penyelesaian :**
**1.** Mengambil nilai HOUR, dimana HOUR merupakan jam saat script dijalankan.
```sh
HOUR=$(date +"%k")
```

**2.** Jika jam saat script dijalankan 00:00, maka jumlah image = 1, jika tidak, jumlah image = hour
```sh
if [ "$HOUR" -eq 0 ]; then
    NUM_IMAGES=1
else
    NUM_IMAGES=$HOUR
fi
```

**3.** Membuat folder dengan format nama yang ditentukan, dengan cara meng-list di dalam directory tersebut, dan mengambil jumlah semua file yang berawalan kumpulan_.
```sh
FOLDER_NAME="kumpulan_$(( $(ls | grep -c ^kumpulan_) + 1 ))"
mkdir "$FOLDER_NAME"
```

**4.** Melakukan Download Image tersebut sebanyak num_images kali, dan setiap iterasi akan dibuat file dengan nama perjalanan_x.
```sh
for (( i=1; i<=$NUM_IMAGES; i++ ))
do
    FILE_NAME="perjalanan_$i.jpg"
    wget -O "$FOLDER_NAME/$FILE_NAME" "https://source.unsplash.com/random?indonesia&orientation=landscape"
done
```

**5.** Pernyataan konfirmasi
```sh
echo "Berhasil mendownload $NUM_IMAGES foto tentang indonesia ke folder $FOLDER_NAME"
```


**6.** cron job untuk menjalankan setiap 10 jam sekali. setiap 10 jam, akan bergerak ke folder tersebut, dan menjalankan sr
```
*/10 * * * cd /path/to/Folders && /bin/bash kobeni_liburan.sh
```

![output_2_1](/uploads/520e2c4b36224a119c44080dd016ae3a/image.png)

![output_2_file](/uploads/515bebb88ab9060e5333a0e1612312a0/image.png)


**b. Men-zip folder yang sudah didownload setiap hari dengan format nama devil_x**

**1.** Buat nama folder sesuai format nama, dengan cara meng-list di dalam directory tersebut, dan mengambil jumlah semua file yang berawalan devil_
```sh
ZIP_NAME="devil_$(( $(ls | grep -c ^devil_) + 1 ))"
```

**2.** Lakukan zip untuk yang berawalan kumpulan_
```sh
zip -r "$ZIP_NAME.zip" kumpulan_*
```

**3.** hapus file kumpulan_ diluar zipped tadi
```sh
rm -r kumpulan_*
```


**4.** Pernyataan konfirmasi
```sh
echo "Zipped folders into $ZIP_NAME.zip"
```

**5.** cron job untuk melakukan setiap hari
```
@daily cd /path/to/Folders && /bin/bash zip.sh
```
![output_2_2](/uploads/3b53b410c1be759636dd7b351dfa6d8e/image.png)
![output_2_file2](/uploads/7143a9eb8d6ac0757b582005436feb05/image.png)


### Soal 3
Peter Griffin hendak membuat suatu sistem register pada script **louis.sh** dari setiap user yang berhasil didaftarkan di dalam file **/users/users.txt**. Peter Griffin juga membuat sistem login yang dibuat di script **retep.sh**. 

**a.**	Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan dengan menggunakan minimal 8 karakter, memiliki minimal 1 huruf kapital dan 1 huruf kecil, alphanumeric, tidak boleh sama dengan username dan Tidak boleh menggunakan kata chicken atau ernie.

**Penyelesaiaan :**

**1.** Membuat **louis.sh** sebagai register. Lalu akan menerima inputan username setelah itu akan dilakukan pengecekan isi file supaya tidak ditemukan username yang sama pada **/users/users.txt**. Juga digunakan delimiter **"/"** untuk membatasi atau memisahkan antara username dan password yang tercatat.
```sh
echo "Masukkan Username Anda :"
read username
if grep -q "^${username}/" /users/users.txt
```
**2.** Jika ditemukan username dan password sama maka akan mengeluarkan message **REGISTER: ERROR User already exists**. lalu akan tercatat pada file **./log.txt**
```sh
echo "Username Tidak Valid !!!"
echo "REGISTER: ERROR User already exists" >> ./log.txt
```
**3.** Jika username telah memenuhi maka akan dilanjutkan untuk mengisikan password**
```sh
echo "Masukkan Password Anda :"
read password
```
**4.** Melakukan pengecekan terhadap password yang telah dimasukkan. Pengecekan ini harus memenuhi beberapa syarat yang telah ditetapkan yaitu : 
```sh
if [[ ${#password} -gt 7 && "$password" =~ [A-Z] &&  "$password" =~ [a-z]  &&  "$password" =~ [0-9]  && "$password" != "$username"  &&  "$password" != *"chicken"*  &&  "$password" != *"ernie"* ]]
```
Memeriksa apakah passwod yang dimasukkan lebih dari 7 karakter
```sh
${#password} -gt 7
```
Memeriksa apakah memiliki upper case
```sh
"$password" =~ [A-Z]
```
Memeriksa apakah memiliki lower case
```sh
"$password" =~ [a-z]
```
Password memiliki kombinasi huruf dan angka **(Alphanumeric)**
```sh
"$password" =~ [0-9]
```
Password tidak boleh sama dengan username
```sh
"$password" != "$username"
```
Password tidak boleh menggunakan kata kata **"chicken"**
```sh
"$password" != *"chicken"*
```
Password tidak boleh menggunakan kata kata **"ernie"**
```sh
"$password" != *"ernie"*
```
**5.** Jika  username dan password telah memnuhi maka akan mengeluarkan waktu register dan message **REGISTER: INFO User (username) registered successfully"**.
```sh
echo "Selamat Password Memenuhi"
echo "date=$(date '+%m-%d-%Y %H:%M:%S') REGISTER: INFO User $username registered successfully" >> ./log.txt

```
**6.** Menambahkan (append) baris baru yang berisi username dan password ke dalam file **/users/users.txt** dengan pemisahnya adalah **/**.
```sh
echo $username/$password >> /users/users.txt
```
**b.**	Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

**Penyelesaian :**

**1.** Membuat **retep.sh** sebagai sistem log-in yang didalamnya memiliki pengecekan isi file supaya tidak ditemukan username yang sama pada **/users/users.txt**.
```sh
if ! grep -q "^${username}/" /users/users.txt; 
```
**2.** Melakukan pencarian password pengguna didalam file **/users/users.txt**. Pencarian dimulai dengan string (text) yang sama dengan isi variable **$username**. Lalu **cut -d'/' -f2** digunakan untuk memotong baris yang cocok dengan username dengan delimeter **"/"**. Setelah itu, Hasil keluaran dari perintah tersebut akan digunakan sebagai argumen dari variabel **userpw**
```sh
userpw=$(grep "^${username}" /users/users.txt | cut -d'/' -f2)
```
**3.** Melakukan pengecekan apakah nilai variable **"password"** tiidak sama dengan nilai **"userpw"**. Jika salah maka akan mengeluarkan waktu login dan message **LOGIN: ERROR Failed login attempt on user**, lalu akan tercatat pada file **./log.txt**
```sh
if [[ "$password" != "$userpw" ]]; then
    echo "Password Salah !!!"
    echo "date=$(date '+%m-%d-%Y %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> ./log.txt
```
**4.** Jika berhasil maka akan mengeluarkan waktu login dan message **LOGIN: INFO User $username logged in** lalu akan tercatat pada file **./log.txt**
```sh
else
    echo "Login Berhasil"
    echo "date=$(date '+%m-%d-%Y %H:%M:%S') LOGIN: INFO User $username logged in" >> ./log.txt
```
**Output :**

Output /Users/Users.txt

![Screenshot_2023-03-10_121056](/uploads/a7231a0c403ffb08ebdce2d81dd0eacd/Screenshot_2023-03-10_121056.png)

Output Log.txt

![Screenshot_2023-03-10_120836](/uploads/8a4070b90de98ee7e3fe234daa672cf4/Screenshot_2023-03-10_120836.png)


### Soal 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File **syslog** tersebut harus memiliki ketentuan :

**a.** Backup file log system dengan format **jam:menit tanggal:bulan:tahun** (dalam format .txt).

**Penyelesaian :** 

Membuat variabel untuk menampung format nama yang akan dituliskan menjadi nama file hasil enkripsi. Pada penyelesaian berikut, digunakan bantuan `date` dan variabel yang diberi nama `backupFile`. Nantinya format nama tersebut akan diberikan ekstensi .txt
```sh
backupFile=`date '+%H:%M %d:%m:%Y'`
```

 **b.** Isi file harus dienkripsi dengan *string manipulation* yang disesuaikan dengan jam dilakukannya backup seperti berikut:
- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
- Setelah huruf z akan kembali ke huruf a

**Penyelesaian :**

1. Untuk menyelesaikan metode enkripsi tersebut diperlukan batuan sebuah *fungsi* yang bertujuan untuk mengubah angka menjadi huruf. Pada penyelesaian berikut digunakan sebuah fungsi yangh diberi nama `chr`. Fungsi `chr` ini akan menerima argumen berupa bilangan desimal, lalu mengkonversi bilangan desimal tersebut menjadi 3 digit bilangan oktal, setelah itu diubah menjadi huruf untuk bilangan oktal tersebut.

```sh
chr()
{
  printf "\\$(printf '%03o' "$1")"
}
```

2. Algoritma enkripsi yang sesuai untuk kasus tersebut memiliki dasar yang sama dengan enkripsi *Caesar Cipher*, yaitu dengan memanfaatkan pergeseran huruf abjad dari urutan sebenarnya. Pergeseran dilakukan dengan menampung nilai jam saat file di-backup ke dalam sebuah variabel bernama `shift`. Variabel `shift` akan mengambil nilai jam dari waktu saat dilakukannya backup. Untuk interval nilai jam pada kasus ini yaitu 0 - 23 (pukul 00 sampai 23).
```sh
shift=`date +%H`
```

3. Setelah melakukan pengambilan nilai pergeseran, berikutnya diperlukan sebuah variabel untuk menampung data **syslog** yang akan dienkripsi. Variabel yang digunakan yaitu `contFile`. Variabel ini akan mengakses langsung direktori dimana **syslog** berada yaitu pada direktori `/var/log/syslog` dengan bantuan perintah `cat`.
```sh
contFile=`cat /var/log/syslog`
```

4. Berikutnya akan dilakukan pemeriksaan terhadap nilai pergeseran `shift`. Jika `shift` bernilai selain 0 (`shift!=0`), maka terjadi pergeseran abjad yang digunakan untuk mengenkripsi isi dari `contFile`. Saat terjadi pergeseran, abjad yang pertama bukan lagi huruf `a`. Huruf abjad pertama akan digantikan dengan huruf yang baru hasil penjumlahan huruf `a` dengan pergeseran `shift`, pada penyelesaian berikut digunakan angka 97 yang mana merupakan bilangan desimal ASCII untuk huruf `a` kecil. Huruf pertama ini ditampung dalam suatu variabel bernama `lcf` yaitu singkatan dari `lowercase_front`. Adapun huruf terakhir juga bukan lagi huruf `z`, melainkan satu huruf sebelum `lcf`. Pada penyelesaian berikut abjad terakhir hasil pergeseran tersebut ditampung dalam variabel `lcb` singkatan dari `lowercase_back`. `lcb` ini adalah satu huruf sebelum `lcf`.
```sh
lcf=$((97 + $shift))
lcb=$(($lcf - 1))
```
5. Setelah diperoleh bilangan desimal untuk masing-masing `lcf` dan `lcb`, yang mana keduanya berturut-turut merupakan huruf pertama dan terakhir dari abjad enkripsi, maka kedua variabel tersebut diubah menjadi huruf abjad dengan bantuan fungsi `chr`. Hasil konversinya dimasukkan ke dalam variabel `fr` dan `bk`. Variabel `fr` dan `bk` masing-masing berisi huruf abjad pertama dan terakhir enkripsi.
```sh
fr=`chr $lcf`
bk=`chr $lcb`
```
6. Tahap berikutnya yaitu mengambil isi dari variabel `contFile` dengan bantuan `printf "%s"` lalu dilakukan `piping` (operasi `pipe`) ke perintah berikutnya yaitu `tr`. Perintah `tr` ini digunakan untuk mengganti huruf abjad biasa dengan abjed enkripsi. Abjad yang sebelumnya `a-z` diganti dengan abjad enkripsi yaitu `fr-za-bk`, begitupun juga dengan huruf kapitalnya, yaitu `A-Z` menjadi `{fr^^}-ZA-{bk^^}`. Tanda `^^` digunakan untuk mengubah huruf `lowercase` menjadi `uppercase`. Hasil perubahan (enkripsi) semua abjad di variabel `contFile` dimasukkan ke variabel baru yaitu `encFile`. Isi dari variabel ini lalu disimpan ke file backup baru dengan nama sesuai format yaitu `backupFile` dan diberikan ekstensi .txt.
```sh
encFile=`printf "%s" "$contFile" | tr "[a-zA-Z]" "[$fr-za-$bk${fr^^}-ZA-${bk^^}]"`
printf "%s"  "$encFile" > "$backupFile".txt
```

7. Jika `shift` bernilai 0, maka dengan kata lain tidak terjadi pergeseran terhadap abjad dan hasil dari enkripsi file **syslog** akan tetap sama dengan sebelumnya (tidak berubah). Dengan begitu, isi dari variabel `contFile` dapat langsung disimpan dengan nama sesuai format.
```sh
printf "$s"  "$contFile" > "$backupFile".txt
```

Berikut dokumentasi eksekusi program log_encrypt.sh

![SS_run_enc](/uploads/2cf12a2481a04aa611e2d72e2dd84c60/SS_run_enc.png)

**c.** Buat juga script untuk dekripsinya.

**Penyelesaian :**

1. Untuk melakukan dekripsi, tetap digunakan bantuan fungsi `chr`. Pada dasarnya dekripsi dilakukan dengan menukar urutan abjad enkripsi yang sebelumnya dengan urutan abjad normal. Penyelesaian dekripsi ini membutuhkan argumen tambahan saat dijalankan melalui terminal. Argumen tersebut yaitu nama file hasil enkripsi. Untuk dekripsi ini isi dari variabel `contFile` diambil dari file hasil enkripsi dalam bentuk argumen, sehingga code-nya menjadi sebagai berikut
```sh
chr()
{
 printf "\\$(printf '%03o' "$1")"
}

contFile=` cat "$1$2"`
```

2. Berikutnya untuk mengembalikan urutan abjad menjadi normal, maka diperlukan nilai pergeseran yang digunakan saat mengenkripsi file tersebut. Nilai pergeseran ini juga disimpan dalam suatu variabel bernama `shift`. Nilai pergeseran diperoleh dari pengambilan argumen pertama nama file dengan bantuan perintah `echo`. Lalu dilakukan `piping` ke perintah `awk` dengan delimiter `:` untuk mengambil kolom pertamanya (`printf $1`).
```sh
shift=`echo "$1" | awk -F: '{printf $1}'`
```

3. Untuk proses perubahan huruf menjadi angka, dilakukan proses yang sama seperti pada code untuk enkripsi
```sh
lcf=$((97 + $shift))
lcb=$(($lcf - 1))
fr=`chr $lcf`
bk=`chr $lcb`
```

4. Berikutnya dilakukan dekripsi dengan bantuan perintah `tr` namun dengan batas interval abjad yang dibalik, dimana huruf enkripsi ditukar dengan abjad normal. Hasil perubahan ini lalu dimasukkan ke dalam variabel bernama `decFile`. Hasil file dekripsi ini diberi nama dengan format yang sama dengan format penamaan enkripsi, namun ditambahkan tulisan `decrypt_` sebelum nama filenya
```sh
decFile=`printf "%s" "$contFile" | tr [$fr-za-$bk${fr^^}-ZA-${bk^^}] [a-zA-Z]`

printf "%s" "$decFile" > "decrypt_$1"
```

Berikut dokumentasi eksekusi program log_decrypt.sh

![SS_run_dec](/uploads/f6239820b8d967499230fa489750a748/SS_run_dec.png)


**d.** Lakukan backup file **syslog** setiap dua jam

**Penyelesaian :**

Untuk melakukan backup yang disertai dengan enkripsi terhadap file **syslog** setiap dua jam, maka dapat digunakan cron jobs sebagai berikut,
```sh
0 */2 * * * /path/to/script/log_encrypt.sh
```

### Kendala
Kendala utama yang kami alami dalam pengerjaan soal shift modul 1 adalah pemahaman kami yang masih belum sempurna **terkhusus dalam hal sintaks**. Hal ini dikarenakan materi yang didapatkan merupakan materi yang relatif baru untuk kami
