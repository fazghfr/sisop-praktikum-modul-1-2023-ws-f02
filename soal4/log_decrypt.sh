#!/bin/bash

chr()
{
 printf "\\$(printf '%03o' "$1")"
}

contFile=` cat "$1$2"`
shift=`echo "$1" | awk -F: '{printf $1}'`

lcf=$((97 + $shift))
lcb=$(($lcf - 1))
fr=`chr $lcf`
bk=`chr $lcb`
decFile=`printf "%s" "$contFile" | tr [$fr-za-$bk${fr^^}-ZA-${bk^^}] [a-zA-Z]`

printf "%s" "$decFile" > "decrypt_$1"
