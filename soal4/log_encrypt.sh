#!/bin/bash

chr()
{
  printf "\\$(printf '%03o' "$1")"
}

shift=`date +%H`
backupFile=`date '+%H:%M %d:%m:%Y'`

contFile=`cat /var/log/syslog`


if (( $shift != 0)) 
then 
    lcf=$((97 + $shift))
    lcb=$(($lcf - 1))
    fr=`chr $lcf`
    bk=`chr $lcb`
    encFile=`printf "%s" "$contFile" | tr "[a-zA-Z]" "[$fr-za-$bk${fr^^}-ZA-${bk^^}]"`
    printf "%s"  "$encFile" > "$backupFile".txt
else
    printf "$s"  "$contFile" > "$backupFile".txt
fi

# cron jobs untuk melakukan eksekusi backup dan enkripsi setiap 2 jam sekali
# 0 */2 * * * /path/to/script/log_encrypt.sh



