#!/bin/bash

# get current hour
HOUR=$(date +"%k")

if [ "$HOUR" -eq 0 ]; then
    NUM_IMAGES=1
else
    NUM_IMAGES=$HOUR
fi

FOLDER_NAME="kumpulan_$(( $(ls | grep -c ^kumpulan_) + 1 ))"
mkdir "$FOLDER_NAME"

for (( i=1; i<=$NUM_IMAGES; i++ ))
do
    FILE_NAME="perjalanan_$i.jpg"
    wget -O "$FOLDER_NAME/$FILE_NAME" "https://source.unsplash.com/random?indonesia&orientation=landscape"
done

echo "Berhasil mendownload $NUM_IMAGES foto tentang indonesia ke folder $FOLDER_NAME"

if [ $(date +"%H") -eq 0 ]; then
	ZIP_NAME="devil_$(( $(ls | grep -c ^devil_) + 1 ))"
	zip -r "$ZIP_NAME.zip" kumpulan_*
	rm -r kumpulan_*
	 echo "Zipped folders into $ZIP_NAME.zip"
fi

# jika script digabung, maka ada dua konfigurasi cron:
# 0 */10 * * * cd /path/to/Folders && /bin/bash kobeni_liburan.sh
# @daily cd /path/to/Folders && /bin/bash kobeni_liburan.sh

#jika script dipisah, untuk download pada downloadd.sh dan zip pada zip.sh
# 0 */10 * * * cd /path/to/Folders && /bin/bash downloadd.sh
# @daily cd /path/to/Folders && /bin/bash zip.sh
