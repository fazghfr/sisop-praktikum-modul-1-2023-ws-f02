#!/bin/bash

#nomor 1, menampilkan 5 universitas teratas di jepang
grep 'JP' dataset.csv | sort -t, -n -k1| head -5 | awk -F ',' 'BEGIN {print "Top 5 University in Japan"} {print $1, $2}'

#nomor 2, menampilkan universitas dengan skor fsr terendah dari hasil nomor 1
grep 'JP' dataset.csv | sort -t, -g -k9| head -5|awk -F',' 'BEGIN{print "\nLowest FSR Score University in Japan"; print "FSR, NAME"}{print $9,$2}' 

#nomor 3, menampilkan 10 universitas ger rank paling tinggi kolom ke 20
grep 'JP' dataset.csv | sort -t, -g -k20 | head -10 | awk -F',' 'BEGIN{print "\nTop 10 university based  on GER rank"}{print $1,$20,$2}'

#nomor 4.
echo -e '\n'
grep 'Keren' dataset.csv | awk -F ',' '{print $1,$2}'
